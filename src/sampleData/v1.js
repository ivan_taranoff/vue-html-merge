export default {
  prev: `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<del></del><font color="#222222">
<font face="Arial, serif">
<repl><font size="3" style="font-size: 12pt">
длинный текст
<br/>
<br/>
</font></repl>
</font>
</font>
<br/>
</p>
`,
  new: `
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff">
<a name="habracut"></a>
<ins><img src="asdasd"/>
</ins><font color="#222222">
<font face="Arial, serif">
<repl><font size="3" style="font-size: 12pt; font-weight: 100">
<b>длин<i>ный</b>текст</i>
<br/>
<br/>
</font></repl>
</font>
</font>
<br/>
</p>
`
};
