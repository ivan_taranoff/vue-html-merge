export default {
  prev: `
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
\t<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
\t<title></title>
\t<meta name="generator" content="LibreOffice 6.2.5.2 (Linux)"/>
\t<meta name="author" content="Makhotkina Viktoriya"/>
\t<meta name="created" content="2019-02-15T14:06:00"/>
\t<meta name="changedby" content="Makhotkina Viktoriya"/>
\t<meta name="changed" content="2019-02-15T14:06:00"/>
\t<meta name="AppVersion" content="15.0000"/>
\t<meta name="Company" content="DigitalDesign asd"/>
\t<meta name="DocSecurity" content="0"/>
\t<meta name="HyperlinksChanged" content="false"/>
\t<meta name="LinksUpToDate" content="false"/>
\t<meta name="ScaleCrop" content="false"/>
\t<meta name="ShareDoc" content="false"/>
\t<style type="text/css">
\t\t@page { size: 8.27in 11.69in; margin-left: 1.18in; margin-right: 0.59in; margin-top: 0.79in; margin-bottom: 0.79in }
\t\tp { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent }
\t\ta:link { color: #0000ff; text-decoration: underline; text-transform: uppercase; }
\t</style>
</head>
<body lang="ru-RU" link="#0000ff" vlink="#800000" dir="ltr"><p style="margin-bottom: 0.11in; line-height: 100%; background: #ffffff">
\t<font color="#343434"><font face="Arial, serif"><b>жирненький
\t\tтекстик <i>курсивчатый</b></font></font></i></p>
<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<repl><a href="https://habr.com/ru/hub/algorithms/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>долор</u></font></font></font></a></repl><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/artificial_intelligence/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>ипсум
\t\t\t<del></del>с<del>и</del>т<del></del> <repl>а</repl>м<del></del>е<del></del>т</u></font></font></font></a><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/maths/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>лорем</u></font></font></font></a><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/machine_learning/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>элемент
\t\t\tсписка</u></font></font></font></a></p>
</ul>
<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<repl><span style="display: inline-block; border: 1px solid #b0b0e2; padding: 0.03in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></repl></p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
\t<font color="#222222"><font face="Arial, serif"><del><font size="3" style="font-size: 12pt">длинный текст<br/>
\t\t<br/>
\t</font></del></font></font><br/>

</p>
<p style="margin-bottom: 0in; line-height: 0.33in; background: #ffffff">
\t<font color="#222222"><font face="Arial, serif"><font size="5" style="font-size: 18pt">тексе</font></font></font></p>

</body>
</html>
`,
  new: `
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
\t<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
\t<title></title>
\t<meta name="generator" content="LibreOffice 6.2.5.2 (Linux)"/>
\t<meta name="author" content="Makhotkina Viktoriya"/>
\t<meta name="created" content="2019-02-15T14:06:00"/>
\t<meta name="changedby" content="Makhotkina Viktoriya"/>
\t<meta name="changed" content="2019-02-15T14:06:00"/>
\t<meta name="AppVersion" content="15.0000"/>
\t<meta name="Company" content="DigitalDesign asd"/>
\t<meta name="DocSecurity" content="0"/>
\t<meta name="HyperlinksChanged" content="false"/>
\t<meta name="LinksUpToDate" content="false"/>
\t<meta name="ScaleCrop" content="false"/>
\t<meta name="ShareDoc" content="false"/>
\t<style type="text/css">
\t\t@page { size: 8.27in 11.69in; margin-left: 1.18in; margin-right: 0.59in; margin-top: 0.79in; margin-bottom: 0.79in }
\t\tp { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent }
\t\ta:link { color: #0000ff; text-decoration: underline; text-transform: uppercase; }
\t</style>
</head>
<body lang="ru-RU" link="#0000ff" vlink="#800000" dir="ltr"><p style="margin-bottom: 0.11in; line-height: 100%; background: #ffffff">
\t<font color="#343434"><font face="Arial, serif"><b>жирненький
\t\tтекстик <i>курсивчатый</b></font></font></i></p>
<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<repl><a href="https://habr.com/ru/hub/algorithms/" target="изменим таргет"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>долор</u></font></font></font></a></repl><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/artificial_intelligence/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>ипсум
\t\t\t<ins>про</ins>с<del></del>т<ins>о</ins> <repl>смени</repl>м<ins> т</ins>е<ins>кс</ins>т</u></font></font></font></a><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/maths/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>лорем</u></font></font></font></a><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt">,</font></font></font></p>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t\t<a href="https://habr.com/ru/hub/machine_learning/" target="адрес ссылки"><font color="#5e6973"><font face="Arial, serif"><font size="2" style="font-size: 10pt"><u>элемент
\t\t\tсписка</u></font></font></font></a></p>
</ul>
<ul>
\t<li><p style="margin-right: 0.08in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
\t<repl><span style="display: inline-block; border: 1px solid #b0b0e2; padding: 2in"><font color="#6667a3"><sub><font face="Arial, serif"><font size="2" style="font-size: 9pt"><u><a href="https://habr.com/ru/sandbox/" target="какой-то таргет">текст
\tссылки</span></u></font></font></sub></font></a></repl></p>
</ul>
<p style="margin-bottom: 0in; line-height: 100%; background: #ffffff"><a name="habracut"></a>
\t<font color="#222222"><font face="Arial, serif"><ins><font size="3" style="font-size: 12pt; font-weight: 100"><b>длин<i>ный</b> текст</i><br/>
\t\t<br/>
\t</font></ins></font></font><br/>

</p>
<p style="margin-bottom: 0in; line-height: 0.33in; background: #ffffff">
\t<font color="#222222"><font face="Arial, serif"><font size="5" style="font-size: 18pt">тексе</font></font></font></p>

</body>
</html>
`
};
